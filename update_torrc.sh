#!/bin/bash

# ExcludeExitNodes
sed -i '/^ExcludeExitNodes*/d' /etc/tor/torrc
echo -n "ExcludeExitNodes " >> /etc/tor/torrc
sed ':a;N;$!ba;s/\n/,/g' ExcludeExitNodes >> /etc/tor/torrc

# ExitNodes
#sed -i '/^ExitNodes*/d' /etc/tor/torrc
#echo -n "ExitNodes " >> /etc/tor/torrc
#sed ':a;N;$!ba;s/\n/,/g' ExitNodes >> /etc/tor/torrc
